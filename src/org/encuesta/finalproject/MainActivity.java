package org.encuesta.finalproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	private EditText usuario;
	private EditText password;
	private Button login;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		usuario = (EditText) findViewById(R.id.campoUsuario);
		password = (EditText) findViewById(R.id.campoPassword);
		login = (Button) findViewById(R.id.botonIniciarSesion);
		login.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		Database db = new Database(this.getBaseContext());
		boolean existe = db.existeUsuario(usuario.getText().toString(), password.getText().toString());
		if	(existe) {
			lanzarActivy(Principal.class);
		} else {
			Toast.makeText(arg0.getContext(),"Error, no existe el usuario", Toast.LENGTH_LONG).show();
		}
		
		
	}

	private void lanzarActivy(Class<Principal> principal) {
		Intent i = new Intent(MainActivity.this, principal );
		i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		String [][] datos = {{"ID_USUARI0","01"},{"FOLIO_ENCUESTADOR","07470078"},{"ID_ENCUESTA","01"},{}}; 
		i.putExtra("DATOS", datos);
		startActivity(i);
		
	}

	

}
