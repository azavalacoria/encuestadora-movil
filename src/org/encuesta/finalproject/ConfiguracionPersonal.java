package org.encuesta.finalproject;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class ConfiguracionPersonal extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configuracion_personal);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.configuracion_personal, menu);
		return true;
	}

}
