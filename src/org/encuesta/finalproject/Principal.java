package org.encuesta.finalproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Principal extends Activity implements OnClickListener{
	Button iniciarEncuesta;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principal);
		iniciarEncuesta = (Button)findViewById(R.id.botonIniciarEncuesta);
		iniciarEncuesta.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.principal, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		lanzarActivity(Vivienda.class);
		
	}

	private void lanzarActivity(Class<Vivienda> vivienda) {
		Intent i = new Intent(Principal.this, Vivienda.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		startActivity(i);
	}

}
